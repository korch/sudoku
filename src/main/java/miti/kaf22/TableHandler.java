package miti.kaf22;

import java.util.Scanner;

/**
 * Created by korch on 25.02.17.
 */
public class TableHandler {
    private static Table table;

    public static void createTable(){
        table = new Table();
        table.showTable();
    }

    public static void HendlerData(){
        Scanner in = new Scanner(System.in);
        int i = in.nextInt();
        int j = in.nextInt();
        int value = in.nextInt();
        if (table.getTable()[i][j] == -1 ){
            table.getTable()[i][j] = value;
        }
        table.showTable();
    }

    /**
     * REAL METHOD
     */
    /*public static boolean isEnd(){
        int sum  = 0;
        boolean [] complete = new boolean[10];
        boolean result =false;

        for (int i = 0; i < table.getHegith(); i++){
            for (int j = 0; j < table.getHegith(); j++){
                sum += table.getTable()[i][j];
            }
            if (sum == 40){
                complete [i] = true;
                Output.slowPrintf("String "+ (i + 1) + " COMPLETE", 75);
            }
        }

        for (int j = 0; j < table.getHegith(); j++){
            for (int i = 0; i < table.getWeigth(); i++){
                sum += table.getTable()[i][j];
            }
            if (sum == 40){
                complete[j+5] =true;
                Output.slowPrintf("Column " + (j + 1) + " COMPLETE", 75);
            }
        }
        for (int i = 0; i < complete.length; i++){
            if (complete[i] == false){
                result =false;
            }
        }

        return result;
    }*/

    /***
     * TEST
     */
    public static boolean isEnd(){
        boolean[] complete = new boolean[5];
        boolean result = true;
        for (int i = 0; i < table.getHegith(); i++) {
            for (int j = 0; j < table.getWeigth(); j++) {
                if (table.getTable()[i][j] != -1) {
                    complete[i] = true;
                } else {
                    complete[i] = false;
                    if (i < table.getHegith()-1) {
                        i++;
                        j=0;
                    }
                }
            }
        }

        for (int i = 0; i < complete.length; i++){
            if (complete[i] == false){
                result = false;
                break;
            }
        }
        return result;

    }


}
