package miti.kaf22;

import java.util.Random;

/**
 * Created by korch on 25.02.17.
 */
public class Table{
    private int weigth;
    private int hegith;
    private int table [][];

    public Table(){
        this.hegith = 5;
        this.weigth = 5;
        table = new int [hegith][weigth];
        completeTable();
    }

    public int[][] getTable(){
        return table;
    }

    public int getHegith(){
        return hegith;
    }

    public int getWeigth(){
        return weigth;
    }

    private void completeTable(){
        int emptyCell = 0;
        Random random = new Random();
        for (int i = 0; i < hegith; i++){
            emptyCell  = random.nextInt(5);
            for (int j = 0; j < weigth; j++){
                if (j != emptyCell){
                    table[i][j] = random.nextInt(10);
                } else {
                    table[i][j] = -1;
                }
            }
        }
    }

    public void showTable(){
        for (int i = 0; i < hegith; i++) {
            for (int j = 0; j < weigth; j++) {
                System.out.printf(table[i][j]+" ");
            }
            System.out.println();
        }
    }



}
