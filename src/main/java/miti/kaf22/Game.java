package miti.kaf22;

import java.util.Scanner;

/**
 * Created by korch on 25.02.17.
 */
public class Game {
    public static void game(){
        Scanner in  = new Scanner(System.in);
        Output.slowPrintf("Welcome to Sudoku player \n", 100);
        TableHandler.createTable();
        do{
            TableHandler.HendlerData();
        } while (TableHandler.isEnd() == false);
        Output.slowPrintf("Game over. You WIN!", 100);
        Output.slowPrintf("If you want continue press enter 1", 100);
        Output.slowPrintf("If you want finish game press enter 2", 100);

        int r = in.nextInt();
        if (r == 1){
            game();
        }
        if (r == 2){
            Output.slowPrintf("The end", 200);
        }
    }
}
