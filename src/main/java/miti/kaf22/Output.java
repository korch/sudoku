package miti.kaf22;

import static java.lang.Thread.sleep;

/**
 * Created by korch on 25.02.17.
 */
public class Output {

    public static void slowPrintf(String printText, int sleepTime){
        for (int i = 0; i < printText.length(); i++){
            System.out.printf(String.valueOf(printText.charAt(i)));
            try {
                sleep(sleepTime);
            } catch (InterruptedException e) {
                System.err.println("Uncorrected set sleepTime \n");
                e.printStackTrace();
            }

        }
        System.out.println();
    }
}
